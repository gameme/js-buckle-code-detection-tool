const {allToAst} = require('./ASTtoDump')

module.exports = {
    summary: "这里是替换AST后的文件",
    * beforeSendResponse(requestDetail, responseDetail) {
        if (requestDetail.url.indexOf('.js') != -1) {
            const newResponse = responseDetail.response;
            newResponse.body = allToAst(newResponse.body.toString());
            return {response: newResponse};
        }
    }
}