const anyproxy = require("anyproxy");
const options = {
    port: 8001,
    rule: require('./rule/astProxy'),
    webInterface: {
        enable: true,
        webProt: 8002
    },
    throttle: 10000,
    forceProxyHttps: true,
    wsIntercept: false,
    silent: false
};

const proxyServer = new anyproxy.ProxyServer(options);

proxyServer.on('ready', () => {
    console.log("开始了")
});
proxyServer.on('error', (e) => {
    console.log("代理错误了")
});
proxyServer.start();


